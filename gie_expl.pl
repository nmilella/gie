%---------------------gestione Spiegazioni-----------------
:-dynamic demandable/1.
:-dynamic min_threshold/1.
:-dynamic irule/3.

min_threshold(0.5).

%data la testa di una regola R e un fatto F
%se il fatto è già stato chiesto va avanti
managersp(R,F):-fact(_,F,_).
%se il fatto è ground, si chiede il suo valore di verità (vero,falso)
managersp(R,F):-ground(F),
				not(fact(_,F,_)),
			    write('Is '),write(F),write('? (yes/no/why)'),nl,
			    write('==> '),
			    read(Rsp),
			    valuersp(R,F,Rsp,0).
%se il fatto non è ground, si chiese se si conoscono gli argomenti mancanti
managersp(R,F):-not(ground(F)),
			    not(fact(_,F,_)),
				write(F),
			    write(', can you complete this relation?(yes/no/why)'),nl,
			    write('==>'),
			    read(Rsp),
			    valuersp(R,F,Rsp,1).
			   
managersp(R,F).


%valuta le risposte dell'utente in caso di fatti ground
%se "no" allora si prosegue
valuersp(R,F,Rsp,0):-Rsp==no.
%se "yes" allora si asserisce il fatto con certezza 1
valuersp(R,F,Rsp,0):-Rsp==yes,
				   write("How confident are you? Type a value from 0 to 1 (eg. 0.6)"),nl,
				   read(Cnf),
					not(fact(_,F,_)),
					check_confidence(Cnf),
				   asserting(F,0,0,Cnf,1,2).
%se "why" allora si spiega perchè è richiesto
%serve alla regola di cui è condizione
%la regola di cui è condizione può servire per altre regole delle quali è a sua volta condizione
valuersp(R,F,Rsp,0):-Rsp==why,
					write('Because it''s useful for knowing: '),
					write(R),nl,
					expl(R),nl,
					managersp(R,F).
					
valuersp(R,F,_,0):-managersp(R,F).					

%valuta le risposte dell'utente in caso di fatti non ground				
%se "no" va avanti
valuersp(R,F,Rsp,1):-Rsp==no.
%se "yes" chiede di riscrivere il fatto completo di tutti gli argomenti
valuersp(R,F,Rsp,1):- Rsp==yes,
					 write('Type complete relation: '),nl,
					 write('==>'),
					 read(Fn),
					 unify_with_occurs_check(F,Fn),
					 write('How confident you are? Type a value from 0 to 1 (eg. 0.6)'),nl,
				     read(Cnf),
					 check_confidence(Cnf),
					 asserting(Fn,0,0,Cnf,1,2).
								   
%se "why" allora si spiega perchè è richiesto
%serve alla regola di cui è condizione
%la regola di cui è condizione può servire per altre regole delle quali è a sua volta condizione
valuersp(R,F,Rsp,1):-Rsp==why,
					write('Because it''s useful for knowing: '),nl,nl,
					write(R),
					expl(R),nl,
					managersp(R,F).
					
valuersp(R,F,_,1):-managersp(R,F).

%utile per la spiegazione complessa
%per ogni regola, attiva la ricerca di tutte le regole di cui è condizione utilizzando l'indicizzazione
expl(R):-irule(Idr,R,C),
		write('itself useful for'),
		nl,
		expl_do(C),
		 !.
	
%data la lista di regole di cui fa parte come condizione


expl_do([]).
%per ogni regola presente nella lista stampa la sua testa e 
%chiama checkrlist per far analizzare la lista di regole di cui a sua volta fa parte
expl_do([Idr|T]):-irule(Idr,R,C),
				  write(R),
			      nl,
				  checkrlist(C),
				  expl_do(T).
			 

checkrlist([]).
%se la lista non è vuota richiama expl_do con la nuova lista
checkrlist(C):-write('itself useful for'),
			   nl,
			   expl_do(C).

%%%%%%%%%%%%%% calcolo del grado di completamento di una condizione %%%%%%%%%%%%%%%%%%%% 
count_cond(C,Ris):-len_op(C,Len),
			 count_cond(C,Len,0,Ris),
			 !.
%%%% and %%%%%
count_cond(and([]),Len,Ve,Ve).
count_cond(and([H|T]),Len,Ve,Ris):-number(H),
									V is Ve + 1 / Len,
									count_cond(and(T),Len,V,Ris).
					  

count_cond(and([H|T]),Len,Ve,Ris):-ver_funct(H),
									len_op(H,L),
									count_cond(H,L,0,R),!,
									V is Ve + R / Len,
									count_cond(and(T),Len,V,Ris).

					  
					  
count_cond(and([H|T]),Len,Ve,Ris):- count_cond(and(T),Len,Ve,Ris).

%%%% or %%%%%
count_cond(or([]),Len,Ve,Ve).
count_cond(or([H|T]),Len,Ve,Ris):- number(H),
							  V is Ve + 1 / Len,
							  count_cond(or(T),Len,V,Ris).
					  

count_cond(or([H|T]),Len,Ve,Ris):- ver_funct(H),
							  len_op(H,L),
							  count_cond(H,L,0,R),!,
							  V is Ve + R / Len,
							  count_cond(or(T),Len,V,Ris).

					  
					  
count_cond(or([H|T]),Len,Ve,Ris):- count_cond(or(T),Len,Ve,Ris).

%%%% no %%%%%

count_cond(no(H),_,_,Ris):-number(X),
						   Ris = 1.
count_cond(no(H),_,_,Ris):-ver_funct(H),
						   len_op(H,L),
						   count_cond(H,L,0,R),
						   !,
						   Ris is R. %modificando
					 
					 
count_cond(no(H),_,_,0).
			 

ver_funct(H):-functor(H,or,_);
			  functor(H,and,_);
			  functor(H,no,_),
		  !.
		  
len_op(or(Cond),L):-length(Cond,L).
len_op(and(Cond),L):-length(Cond,L).
len_op(no(Cond),1).

%---------------------------------ricerca dei fatti chiedibili all'utente--------------------------------------------
%check_demandable(testa, condizione)
%utilizzata in gie_forward_confl//chekcrest_do

check_demandable(R,and([])).
%verifica se F è un fatto chiedibile
check_demandable(R,and([F|T])):-demandable(F),
								managersp(R,F),
								check_demandable(R,and(T)).
%verifica se F è una sotto condizione
check_demandable(R,and([F|T])):-check_demandable(R,F),
								check_demandable(R,and(T)).
check_demandable(R,and([F|T])):-check_demandable(R,and(T)).

%come per l'and
check_demandable(R,or([])).
check_demandable(R,or([F|T])):-demandable(F),
							   managersp(R,F),
							   check_demandable(R,or(T)).
							   
check_demandable(R,or([F|T])):-check_demandable(R,F),
							   check_demandable(R,or(T)).	
							   
check_demandable(R,or([F|T])):-check_demandable(R,or(T)).

%come per l'and
check_demandable(R,no(F)):-demandable(F),
						   managersp(R,F).
check_demandable(R,no(F)):-check_demandable(F).
check_demandable(R,no(F)).


check_confidence(0):- write('Value 0 means you are not confident'),nl.
check_confidence(Cnf):- Cnf > 0,
						Cnf < 0.5,
						write('Warning: Value '),write(Cnf),write(' means you are not enough confident.'),nl.
check_confidence(Cnf):- Cnf >= 0.5.