%Author: Nicola Milella
%Mat: 585459

%-------------------------------------utilities per il calcolo del minimo e del massimo di liste normali e non------------------------------
%--------------calcolo del minimo------------
get_min([X],Min):- Min = X,
					number(Min).
get_min([X],Min):- Min = 0.

get_min([and(L),Y|T],Min):-!,
						   get_min(L,M),
						   get_min([M,Y|T],Min).
get_min([X,and(L)|T],Min):-!,
						   get_min(L,M),
						   get_min([X,M|T],Min).
get_min([or(L),Y|T],Min):-!,
						  get_max(L,M),
						  get_min([M,Y|T],Min).
get_min([X,or(L)|T],Min):-!,
						  get_max(L,M),
						  get_min([X,M|T],Min).
get_min([no(L),Y|T],Min):-!,
						  M is 1 - L,
						  get_min([M,Y|T],Min).
get_min([X,no(L)|T],Min):-!,
						  M is 1 - L,
						  get_min([X,M|T],Min).

get_min([X,Y|T],Min):-not(number(X)),
					  functor(X,N,_),
					  N\==and,
					  N\==or,
					  N\==no,
					  get_min([Y|T],Min).
get_min([X,Y|T],Min):-not(number(Y)),
					  functor(Y,N,_),
					  N\==and,
					  N\==or,
					  N\==no,
					  get_min([X|T],Min).

get_min([X,Y|T],Min):- X<Y,
					   !,
					   get_min([X|T],Min).
get_min([X,Y|T],Min):-get_min([Y|T],Min).

%--------------calcolo del massimo------------
get_max([X],Max):- Max = X,
					number(Max).
get_max([X],Max):- Max = 0.
%get_max([and(L)],Max):-!,get_min([and([L,2])],M),
%					   get_max([M],Max).

get_max([and(L),Y|T],Max):-!,
						   get_min(L,M),
						   get_max([M,Y|T],Max).
get_max([X,and(L)|T],Max):-!,
						   get_min(L,M),
						   get_max([X,M|T],Max).
get_max([or(L),Y|T],Max):-!,
						  get_max(L,M),
						  get_max([M,Y|T],Max).
get_max([X,or(L)|T],Max):-!,
						  get_max(L,M),
						  get_max([X,M|T],Max).
get_max([no(L),Y|T],Max):-!,
						  M is 1 - L,
						  get_max([M,Y|T],Max).
get_max([X,no(L)|T],Max):-!,
						  M is 1 - L,
						  get_max([X,M|T],Max).

get_max([X,Y|T],Max):-not(number(X)),
					  functor(X,N,_),
					  N\==and,
					  N\==or,
					  N\==no,
					  get_max([Y|T],Max).
get_max([X,Y|T],Max):-not(number(Y)),
					  functor(Y,N,_),
					  N\==and,
					  N\==or,
					  N\==no,
					  get_max([X|T],Max).

get_max([X,Y|T],Max):- X>Y,
					   !,
					   get_max([X|T],Max).
get_max([X,Y|T],Max):-get_max([Y|T],Max).

%-----------------------------------utilities aggiornamento degli "id" di fatti e regole-----------------------------------
%-------------regole-----------------
%crea la lista di tutti gli id dei fatti presenti nella kb
idrlist:-rule(Idr,_,_,_,_),
		 idr(L),
		 append([Idr],L,Ln),
		 retract(idr(L)),
		 assert(idr(Ln)),
		 fail.
idrlist.

%aggiunge il nuovo id alla lista
idrlist_upd(Idr):-idr(L),
				  append([Idr],L,Ln),
				  retract(idr(L)),
				  assert(idr(Ln)).

%calcola l'ultimo id 
maxidr(M):-idr(L),get_max(L,M).

%-------------fatti------------------
%crea la lista di tutti gli id dei fatti presenti nella kb
%inglobata in itialiafacts
idflist:-fact(Idf,_,_),
		 idf(L),
		 append([Idf],L,Ln),
		 retract(idf(L)),
		 assert(idf(Ln)),
		 fail.
idflist.
%aggiunge il nuovo id alla lista
idflist_upd(Idf):-idf(L),
				  append([Idf],L,Ln),
				  retract(idf(L)),
				  assert(idf(Ln)).
%calcola l'ultimo id 
maxidf(M):-idf(L),get_max(L,M).

%------------------------------------------varie--------------------------------------
%data una condizione, restituisce la stessa inizializzata con lista vuota
getfunct(X,F):-functor(X,N,_),
			   N==and,
			   F=and([]),!.
getfunct(X,F):-functor(X,N,_),
			   N==or,
			   F=or([]),!.
getfunct(X,F):-functor(X,N,_),
			   N==no,
			   F=no(0),!.	

%----------------------ordinamento lista di [id,prior]----------------------------------------
sort_list(L,Sorted):-exchange(L,L1),
					!,
					sort_list(L1,Sorted).
					
% nessuno scambio e' possibile
% la lista e' sorted

sort_list(Sorted,Sorted).

% X e Y vanno scambiati
exchange([[Ix,X],[Iy,Y]|Rest],[[Iy,Y],[Ix,X]|Rest]):- X < Y.

% si cerca di fare uno scambio nella coda

exchange([[Ix,X]|Rest],[[Ix,X]|Rest1]) :- exchange(Rest,Rest1).

%----------------------reinizializzazione ambiente-----------------------
%rimuove tutti i fatti asseriti, lasciando quelli originali
initializefacts([]):-retractall(used_fact(Idf,F,Crt)),
					 retract(idf(L)),
					 assert(idf([])).
					 
initializefacts([Idf|T]):-used_fact(Idf,F,Crt),
						  asserta(fact(Idf,F,Crt)),
						  initializefacts(T).

%rimuove tutte le regole asserite, lasciando quelle originali
initializerules:-rule(Idr,_,_,_,_),
				 initrules(I),
				 not(member(Idr,I)),
				 retract(rule(Idr,_,_,_,_)),
				 fail.
initializerules:-retractall(used_rule(_,_,_,_,_)),
				 retractall(irule(_,_,_)),
				 retract(idr(L)),
				 assert(idr([])).
%------------------------Inizializzazione ambiente----------------------
%crea la lista contenente gli id delle regole iniziali
initrules([]).

initialrules:-  rule(Idr,R,C,P,Crt),
				initrules(I),
				idr(L),
				append([Idr],L,Ln),
				retract(idr(L)),
				assert(idr(Ln)),
				append([Idr],I,In),
				retract(initrules(I)),
				assert(initrules(In)),
				fail.
initialrules:-initrules(I),
			  get_max(I,M),
			  assert(maxinitialidr(M)),!.


%crea la lista contenente gli id dei fatti iniziali
initfacts([]).

initialfacts:-fact(Idf,F,Crt),
			  initfacts(I),
			  idf(L),
			  append([Idf],L,Ln),
			  retract(idf(L)),
			  assert(idf(Ln)),
			  append([Idf],I,In),
			  retract(initfacts(I)),
			  assert(initfacts(In)),
			  fail.
initialfacts:-initfacts(I),
			  get_max(I,M),
			  assert(maxinitialidf(M)),!.
%----------------------Salvataggio nuovi fatti nel file della KB corrente------------------

save:-curr_kb(Kb),
	  open(Kb,append,Stream),
	  initfacts(If),
	  write(Stream,'%New facts'),
	  nl(Stream),
	  write_fact(Stream,If),
	  close(Stream).

			   
write_fact(Stream,If):-
			   used_fact(Idf,F,Cf),
			   not(member(Idf,If)),
			   write(Stream,'fact('),
			   write(Stream,Idf),
			   write(Stream,', '),
			   write(Stream,F),
			   write(Stream,', '),
			   write(Stream,Cf),
			   write(Stream,').'),
			   nl(Stream),
			   fail.
write_fact(Stream,If).
