%Author: Nicola Milella
%Mat: 585459
%------------------------------------sostituzione dei valori di certezza nelle condizioni-----------------------------------------
%-------------------------------and-------------------------------
%Dati:
%condizione -> and([c1,c2,..,cn]),
%fatto -> F, 
%certezza -> Crt,
%Si aggiornerà -> and(Nc)
%Si restituiranno:
%condizione aggiornata -> C
%valore boolean -> B (indica se la condizione è verificata(1) o meno(0))
%Utilizzata in gie_forward_confl//checkfact

updatecond(and([]),F,Crt,and(Nc),B,C):-	C=and(Nc),
										verify(and(Nc),B).

updatecond(and([F|T]),F,Crt,and(Nc),B,C):-append(Nc,[Crt],Ln),
										 updatecond(and(T),F,Crt,and(Ln),B,C).
								
updatecond(and([X|T]),F,Crt,and(Nc),B,C):-number(X),
								         append(Nc,[X],Ln),
								         updatecond(and(T),F,Crt,and(Ln),B,C).
								
updatecond(and([X|T]),F,Crt,and(Nc),B,C):-updatecond(X,F,Crt,Nl,Bn,Cn),
										 append(Nc,[Cn],Ln),
								         updatecond(and(T),F,Crt,and(Ln),B,C).

								
updatecond(and([X|T]),F,Crt,and(Nc),B,C):-append(Nc,[X],Ln),
								         updatecond(and(T),F,Crt,and(Ln),B,C).

%-------------------------------or----------------------------------
%come per l'and
updatecond(or([]),F,Crt,or(Nc),B,C):-	C=or(Nc),
										verify(or(Nc),B).

updatecond(or([F|T]),F,Crt,or(Nc),B,C):-append(Nc,[Crt],Ln),
									   updatecond(or(T),F,Crt,or(Ln),B,C).
								
updatecond(or([X|T]),F,Crt,or(Nc),B,C):-number(X),
								       append(Nc,[X],Ln),
								       updatecond(or(T),F,Crt,or(Ln),B,C).
								
updatecond(or([X|T]),F,Crt,or(Nc),B,C):-updatecond(X,F,Crt,Nl,Bn,Cn),
								       append(Nc,[Cn],Ln),
								       updatecond(or(T),F,Crt,or(Ln),B,C).

								
updatecond(or([X|T]),F,Crt,or(Nc),B,C):-append(Nc,[X],Ln),
								       updatecond(or(T),F,Crt,or(Ln),B,C).
							  
%-------------------------no---------------------------
%come per and e or ma non si effettuano cicli 					  
updatecond(no(F),F,Crt,no(_),B,C):-C=no(Crt),
								  verify(C,B),
								  !.

updatecond(no(X),F,Crt,no(_),B,C):-number(X),
								  C=no(X),
								  verify(C,B),
								  !.
								  
							  
						
updatecond(no(X),F,Crt,no(_),B,C):-updatecond(X,F,Crt,Nl,Bn,Cn),
								  C=no(Cn),
								  verify(C,B),
								  !.
							  
		
updatecond(no(X),F,Crt,no(_),B,C):-C=no(X),
					  			  verify(C,B),
								  !.
							  

%----------------------------------verificare la condizione-----------------------------------------------------
%verifica
%versione vero falso
verify(C):-call(C),!.

%versione con numero di ritorno: vero=1, falso=0
%Input
%C -> condizione
%Output
%V -> 1 vero, 0 falso

verify(C,V):-call(C),
			 !,
			 V=1.
verify(C,V):-V=0.

%data la condizione, verifica se è vera o falsa

and([]).
%verifica se X è un numero 
and([X|T]):-number(X),
			X>0,
			X=<1,
			!,
			and(T).
%verifica se X è una sotto condizione di tipo Or
and([X|T]):-functor(X,or,_),
			call(X),
			!,
			and(T).
%verifica se X è una sotto condizione di tipo And
and([X|T]):-functor(X,and,_),
			call(X),
			!,
			and(T).
%verifica se X è una sotto condizione di tipo No
and([X|T]):-functor(X,no,_),
			call(X),
			!,
			and(T).
%and([X|T]):-demandable(X),
%			ground(X),
%			ask(X,Rsp),
%			manage_rsp(Rsp),
%			!,
%			and(T).
and([_]):-fail.


%come per l'and
%termina l'esecuzione non appena un elemento è verificato
%se si arriva ad esaminara tutti gli elementi della lista vuol dire che nessuno si è verificato e quindi si porta ad un fallimento
or([]):-fail.
or([X|T]):-number(X),
		   X>0,
		   X=<1,
		   !.
or([X|T]):-functor(X,or,_),
		   call(X).
or([X|T]):-functor(X,and,_),
		   call(X).
or([X|T]):-functor(X,no,_),
		   call(X).
%or([X|T]):-demandable(X),
%			ground(X),
%			ask(X,Rsp),
%			manage_rsp(Rsp),
%			!.

or([X|T]):-or(T).


%come per l'and
%l'elemento da verificare è uno, se si verifica vuol dire che la condizione è falsa e quindi si porta ad un fallimento
% altrimenti se l'elemento non è verificato vuol dire che la condizione è vera
no(X):-number(X),
	   X>0,
	   X=<1,
	   !,
	   fail.
no(X):-functor(X,or,_),
	   call(X),
	   !,
	   fail.
no(X):-functor(X,and,_),
	   call(X),
	   !,
	   fail.
no(X):-functor(X,no,_),
	   call(X),
	   !,
	   fail.
%no(X):-demandable(X),
%		ground(X),
%		ask(X,Rsp),
%		manage_rsp(Rsp),
%		!,
%		fail.
no(X).
%recupera il valore massimo in una condizione di tipo Or
get_val(or(C),Val,_):- get_max(C,Val),!.
			  
%recupera il valore minimo in una condizione di tipo And
get_val(and(C),Val,_):- get_min(C,Val),!.
			  
%se la condizione è verificata restituisce 1 , se non è verificata restituisce (1 - certezza)
get_val(no(C),1,1).
%caso in cui la condizione non è verificata
get_val(no(C),Val,0):-number(C),
					  Val is 1-C.

%restituisce la lista di elementi della condizione
get_cond(or(L),L).
get_cond(and(L),L).
get_cond(no(L),L).