%Author: Nicola Milella
%Mat: 585459
%-------------------------indicizzazione regole-------------------------

%indicizzazione regole iniziali
%asserisce irule(Id regola, testa, lista condizioni) per ogni regola inizialmente nella kb
%indicizza le regole

initindexing:-rule(Idr,R,_,_,_),
			  not(irule(Idr,R,_)),
			  assert(irule(Idr,R,[])),
			  index(Idr,R),
			  fail.
initindexing.

%per ogni regola avvia l'indicizzazione
indexing:-rule(Idr,Rt,_,_,_),
		  irule(Idr,Rt,Lc),
		  index(Idr,Rt),
		  fail.
indexing.


				 
%data una regola con id "Idr" e testa "R"  avvia la verifica della presenza nelle condizioni di altre regole
index(Idr,R):-  rule(Idrn,_,C,_,_),
				 Idr\==Idrn,%si potrebbero considerare anche 
				 ver(Idr,Idrn,R,C),
				 fail.
index(Idr,R).

%verifica della presenza nelle condizioni di altre regole
%Idrn e C sono l'id e la condizione della regola da verificare
ver(Idr,Idrn,R,C):-  memb(R,C),!,
					 add(Idr,Idrn,R).
			  
			  
%verifica appartenenza, simile al member della libreria lists, adattato a ricercare nelle condizioni complesse		  
memb(R,and([R|T])).
memb(R,and([X|T])):-memb(R,X),!.
memb(R,and([X|T])):-memb(R,and(T)).

memb(R,or([R|T])).
memb(R,or([X|T])):-memb(R,X),!.
memb(R,or([X|T])):-memb(R,or(T)).

memb(R,no(R)).

%aggiunge l'id della regola Idrn nella lista della regola Idr
add(Idr,Idrn,R):-irule(Idr,R,Lc),
				 !,
				 not(member(Idrn,Lc)),
			     append([Idrn],Lc,Lcn),
			     retract(irule(Idr,R,Lc)),
			     assert(irule(Idr,R,Lcn)).
add(Idr,Idrn,R).

%-------------------------------------------------------




	