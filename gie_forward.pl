%Author: Nicola Milella
%Mat: 585459

%--------------------------------------------------------------motore forward-----------------------------------------------------
:-dynamic fact/3.
:-dynamic rule/5.
:-dynamic frule/5.
:-dynamic used_rule/5.
:-dynamic idf/1.
:-dynamic idr/1.
:-dynamic demandable/1.
:-dynamic confl/1.
:-dynamic fireset/1.
:-dynamic initrules/1.
:-dynamic initfacts/1.
:-dynamic irule/3.
:-dynamic verified/2.
%caricamento dei file contenenti le varie funzionalità
:-reconsult('gie_cond.pl').
:-reconsult('gie_utility.pl').
:-reconsult('gie_indexing.pl').
:-reconsult('gie_confl.pl').
:-reconsult('gie_expl.pl').

%caricamento manuale delle 2 KB presenti
%:-reconsult('gie_SEDUCE_kb.pl').
%:-reconsult('gie_kb.pl').

%%%%%%%%%%%%%Denominazioni ricorrenti%%%%%%%%%%%%%%
%%%%%%fact(Idf,F,Cf)%%%%%%
%Idf -> id fatto
%F -> fatto es. p(a)
%Cf -> certezza fatto 0<Cf<1

%%%%%%rule(Idr,R,C,P,Crt)
%Idr -> id regola
%R -> testa regola p(A,B)
%C -> condizione es. or([p(A),..,and([q(A)..]),..,no(g(A))])
%P -> priorità regola
%Crt -> certezza regola

%B -> boolean 0,1 
%Val -> valore di certezza recuperato da una condizione
%%%%%%%%%%%%%%%%%%%%%%%% ciclo principale %%%%%%%%%%%%%%%%%%%%%%%%%%

%ciclo principale 
mainloop:-fact(_,_,_),
		  forward,
		  write('forward finished'),nl,
		  fireset,
		  fire,
		  write('fire finished'),nl,
		  mainloop,
		  !.
		  
mainloop:-set_threshold,
		  checkrest,
		  fact(_,_,_),
		  mainloop.
		  
mainloop:- write('No more facts to be analyzed'),
			nl.

%%%%%%%%%%%%%%%%%%%%%%%%%% motore forward %%%%%%%%%%%%%%%%%%%%%%%%		   
%verifica che non ci sono più fatti da analizzare		  
forward:-not(fact(Idf,F,Cf)),
		 write('stop forward '),
		 nl,
		 !.

%verifica ogni fatto in ciascuna regola, sfruttando l'indicizzazione, dopo di che lo riscrive come used_fact
forward:-write('forward '),nl,
		 fact(Idf,F,Cf),
		 doall(checkrule(F,Cf)),
		 retract(fact(Idf,F,Cf)),
		 assert(used_fact(Idf,F,Cf)),
		 forward,!.


%verifica se il fatto deriva da una regola indicizzata		 
checkrule(F,Cf):-irule(Id,F,L),
				 !,
				 checkrule(F,Cf,L).

%seleziona tutte le regole e inizia la verifica, senza indicizzazione
checkrule(F,Cf):- rule(Idr,R,C,P,Crt),
			 checkfact(F,Cf,Idr,R,C,P,Crt).

%seleziona le regole in cui il fatto è presente nelle condizioni				
checkrule(_,_,[]).
checkrule(F,Cf,[Idr|T]):-rule(Idr,R,C,P,Crt),
						 checkfact(F,Cf,Idr,R,C,P,Crt),
						 checkrule(F,Cf,T).
						 

				
		  
%verifica se il fatto in considerazione e presente o meno nella regola in considerazione	
%aggiorna(cancella e riasserisce) le regole non verificate, a meno di quelle iniziali
  
checkfact(F,Cf,Idr,R,C,P,Crt):-	 maxinitialidr(MIdr),
								 Idr > MIdr,
								 retract(rule(Idr,_,_,_,_)),
								 getfunct(C,Funct),
								 updatecond(C,F,Cf,Funct,B,Nc),
								 !,
								 %get_val(Nc,Val,B), %non serve più perchè i fatti si asseriscono dopo
								 asserting(R,Nc,P,Crt,_,B).

checkfact(F,Cf,Idr,R,C,P,Crt):-	 getfunct(C,Funct),
								 updatecond(C,F,Cf,Funct,B,Nc),
								 !,
								 %get_val(Nc,Val,B), %non serve più perchè i fatti si asseriscono dopo
								 asserting(R,Nc,P,Crt,_,B). 

%riasserisce una regola
asserting(R,C,P,Crt,_,B):-  B\==2,
							not(rule(_,R,C,P,Crt)),
							maxidr(M),
							Idr is M+1,
							idrlist_upd(Idr),
							!,
							assert(verified(Idr,B)),%%%%%%%%%%%novità%%%%%%%%%%%
							assert(rule(Idr,R,C,P,Crt)).
							
%si asserisce la testa della regola come fatto
asserting(R,_,_,Crt,Val,2):-  
							maxidf(M),
							Idf is M+1,
							idflist_upd(Idf),
							!,
							Crtn is Val*Crt,
							%assert(fact(Idf,R,Crtn)),
							check_certainty(Idf,R,Crtn).
							
check_certainty(_,_,0):- write('Error: this fact will not be asserted, 0 means false.'),nl.

check_certainty(Idf,R,Crt):- Crt > 1,
							write('Error: '),
							write('fact('),write(Idf),write(', '),write(R),write(', '),write(Crt),
							write(') has wrong certainty value. It will not be asserted.').
check_certainty(Idf,R,Crt):- Crt < 0,
							write('Error: '),
							write('fact('),write(Idf),write(', '),write(R),write(', '),write(Crt),
							write(') has wrong certainty value. It will not be asserted.').
check_certainty(Idf,R,Crt):-assert(fact(Idf,R,Crt)).

%verifica che sono state provate tutte le regole		
doall(P):- not(allchecked(P)).

%sfrutta il backtracking per provare tutte le regole
allchecked(P):- call(P),fail.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% fine motore forward %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%% preparazione regole da passare alla componente di conflict resolution %%%%%%%%%%%%%%%%%%%%%%
fireset([]).

%divide le regole pronte a scattare da quelle non pronte
fireset:- write('fireset '),nl,
			rule(Idr,R,C,P,Crt),
			verified(Idr,1),
			fireset_do(Idr,R,C,P,Crt),
			%indexing,
		 fail.
%fireset:-unfireset.
fireset.

%date tutte le informazioni di una regola pronta a scattare:
%aggiorna il fireset
%la riasserisce come frule	 
fireset_do(Idr,R,C,P,Crt):-
						   fireset(L),
						   append([Idr],L,Ln),
						   retract(fireset(L)),
						   assert(fireset(Ln)),
						   retract(rule(Idr,R,C,P,Crt)),
						   assert(frule(Idr,R,C,P,Crt)),
						   !.
						   
unfireset:-write('unfireset '),nl,
			rule(Idr,R,C,P,Crt),
			verified(Idr,1),
			unfireset_do(Idr,R,C,P,Crt),
			%indexing,
		 fail.
unfireset.

%date tutte le informazioni di una regola non pronta a scattare:
%controlla se è già indicizzata								 
unfireset_do(Idr,R,C,P,Crt):-irule(Idr,R,_),
						   write(Idr),
						   write('gia presente'),
						   nl,
						   !.

%non utilizzata						   
%date tutte le informazioni di una regola non pronta a scattare e non indicizzata:
%asserisce il predicato irule(Id,testa,lista regole in cui è presente) utile per l'indicizzazione successiva					 
unfireset_do(Idr,R,C,P,Crt):-assert(irule(Idr,R,[])),!.
unfireset_do(Idr,R,C,P,Crt).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% operazioni post cicli forward per trovare fatti chiedibili all'utente %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%controlla le regole non scattate in cerca di nuovi fatti chiedibili all'utente
checkrest:-rule(Idr,R,C,P,Crt),
		   initrules(I),
		   not(member(Idr,I)),%salta le regole caricate inizialmente
		   count_cond(C,Ris),
		   min_threshold(Trs),
		   Ris >= Trs, %soglia minima di completamento di una condizione
		   checkrest_do(R,C),
		   fail.
		   
checkrest.

checkrest_do(R,C):-check_demandable(R,C),
					 %verify(C,B),%si usa questa variante a 2 parametri di verify perchè non fallisce mai
					 !.   

set_threshold:-min_threshold(Trs),
			   write('The default threshold for considering the conditions is: '),
			   write(Trs),
			   nl,
			   write('Would you change it? yes/no/why'),nl,
			   read(Answ),
			   set_threshold(Answ).
				   
set_threshold(yes):-write('Type new threshold value(0,0.1,..,0.9,1)'),
						read(Trs),
						retractall(min_threshold(_)),
						assert(min_threshold(Trs)),
						write('Threshold updated!'),nl,nl.
						
set_threshold(why):-write('--Increasing the threshold, you set a stricter constraint. The system will consider less rules than normal, having the conditions more chances to be satisfied'),nl,
					write('--Decreasing the thresold, you set a weaker constraint. The system will consider more rules than normal, having the conditions less chances to be satisfied'),nl,nl,
					set_threshold.
set_threshold(_).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						
