%Author: Nicola Milella
%Mat: 585459
%--------------------------------------conflict resolution----------------------

%gestore principale della componente che avvia in ordine tutte le varie operazioni
fire:-assert(confl([])),
	  firerule,
	  asserting,
	  indexing,
	  retractall(verified(_,_)),
	  !.



%per ogni regola avvia la ricerca di tutte le altre che vanno in conflitto
firerule:-write('firerule '),nl,
		  frule(Idr,R,C,P,Crt),
		  firerule_do(Idr,R,P),
		  asserta(confl([])),
		  fail.
firerule:-retract(confl([])).

%date tutte le informazioni della regola:
%si raggruppano le regole in conflitto con lo stesso predicato di testa in confl([[idr1,priorità],...,[idrN,priorità]]) 
firerule_do(Idr,R,P):- fireset(T),
					   confl(L),
					   member(Idr,T),%se una regola non fa parte del fireset vuol dire che è stata già esaminata
					   append([[Idr,P]],L,Ln),
					   retract(confl(L)),
					   asserta(confl(Ln)),
					   delete(T,Idr,Tn),
					   retract(fireset(T)),
					   assert(fireset(Tn)),
					   conflicts(Idr,R),
					   !.
 
%considera le regole pronte a scattare con lo stesso predicato di testa
conflicts(Idr,R):-frule(Idrn,Rn,Cn,Pn,Crtn),
				  Idr\==Idrn,
				  functor(Rn,N,_),
				  functor(R,N,_),
				  %ground(Rn),
				  conflicts_do(Idrn,Pn),
				  fail.
conflicts(_,_).		
			
%raggruppa tutte le regole con lo stesso predicato di testa memorizzando Id e Priorità in confl([[Idr,P],...,[Idrn,Pn]])
%aggiorna il fireset rimuovendo la regola in esame
conflicts_do(Idrn,Pn):-	confl(L),!,
						fireset(T),
						append([[Idrn,Pn]],L,Ln),
						retract(confl(L)),
						asserta(confl(Ln)),
						delete(T,Idrn,Tn),
						retract(fireset(T)),
						assert(fireset(Tn)).
					
asserting:-write('asserting '),nl,
		   confl(L),
		   sort_list(L,Ln),
		   ass_rule(L),
		   retract(confl(L)),
		   fail.
asserting.



					
%gestisce il caso in cui la testa della regola è già un fact
%se il fatto già presente nella WM ha certezza inferiore a quello che si sta asserendo
%viene sostituito dal nuovo con certezza superiore, evitando duplicati

ass_rule([[Idr,P]|T]):- frule(Idr,R,C,P,Crt),
						fact(Idf,R,Crtf),
						get_val(C,V,_),
						Crtn is V*Crt,
						sweep_fact(fact(Idf,R,Crtf),fact(Idf,R,Crtn),Crtf,Crtn),
						retract(frule(Idr,R,C,P,Crt)),
						retract(irule(Idr,R,_)),
						assert(used_rule(Idr,R,C,P,Crt)),%si inserisce tra quelle usate 
						ass_rule(T). %permette di considerare tutte le regole in conflitto, invece che solo la prima
						
%gestisce il caso in cui la testa della regola è un used_fact	
%si ignora 
ass_rule([[Idr,P]|T]):- frule(Idr,R,C,P,Crt),
						used_fact(Idf,R,Crtf),
						retract(frule(Idr,R,C,P,Crt)),
						retract(irule(Idr,R,_)),
						%assert(used_fact(Idfn,R,Crtn)),
						assert(used_rule(Idr,R,C,P,Crt)),%si inserisce tra quelle usate 
						ass_rule(T).
%caso base in cui bisogna asserire la testa della regola come fatto						
ass_rule([[Idr,P]|T]):- frule(Idr,R,C,P,Crt),
						get_val(C,V,_),
						ass_rule_do(Idr,R,C,P,Crt,V),
						ass_rule(T).
						
%%%%%% VARIANTE:se si commenta ass_rule(T) si blocca il ciclo permettendo l'asserzione della regola a priorità maggiore invece che di tutte							   

%esegue l'asserzione della testa della regola
%rimuove la regola e la riscrive come used_rule
ass_rule_do(Idr,R,C,P,Crt,V):-
						asserting(R,_,_,Crt,V,2),
						retract(frule(Idr,R,C,P,Crt)),
						assert(used_rule(Idr,R,C,P,Crt)),
						!.
						
%confronta i valori di certezza del vecchio e nuovo fatto 
%in modo da mantenere in memoria sempre quello con certezza maggiore						
sweep_fact(F,Fn,Crtf,Crtn):- Crtn=<Crtf.
sweep_fact(F,Fn,Crtf,Crtn):- C is Crtn-Crtf,
							 C >= 0.3,%%%% scarto minimo tra le certezze di 2 fatti
							 retract(F),
							 assert(Fn).
sweep_fact(_,_,_,_).