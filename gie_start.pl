%Author: Nicola Milella
%Mat: 585459

:-use_module(library(lists)).
:-dynamic curr_kb/1.
:-dynamic fact/3.
:-dynamic rule/5.
:-dynamic frule/5.
:-dynamic used_rule/5.
:-dynamic idf/1.
:-dynamic idr/1.
:-dynamic demandable/1.
:-dynamic confl/1.
:-dynamic fireset/1.
:-dynamic initrules/1.
:-dynamic initfacts/1.
:-dynamic irule/3.
:-dynamic verified/2.
%%%%:-dynamic ufrule/5.

%carica il file del motore inferenziale che a sua volta avvierà il caricamento di tutti gli altri
:-reconsult('gie_forward.pl').


idf([]).
idr([]).



heading :- nl,
                       write('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'),nl,
                       write('@                                UNIBA                                  @'),nl,
					   write('@                        ARTIFICIAL INTELLIGENCE                        @'),nl,
                       write('@-----------------------------------------------------------------------@'),nl,
                       write('@                    GIE - Generic Inferential Engine                   @'),nl,
                       write('@                         Nicola Milella 585459                         @'),nl,
					   write('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'),nl.
					   
start:- 
		heading,
		menu,
		!. 
					   
menu:- 
			nl,write('Choose action please: '),
			nl,nl,write('1 - Load Knowledge Base. '),
			nl,nl,write('2 - Start engine and infer. '),
			nl,nl,write('3 - View facts. '),
			nl,nl,write('4 - View used facts. '),
			nl,nl,write('5 - View rules. '),
			nl,nl,write('6 - View used rules. '),
			nl,nl,write('7 - View indexed rules. '),
			nl,nl,write('8 - Initialize enviroment. '),
			nl,nl,write('9 - Save new facts to KB. '),
			nl,nl,write('0 - Exit. '),
			nl,nl,write('Type number: '),
			read(Chs),
			do(Chs).
			
			
do(1):- write('Insert complete knowledge base path please: '),nl,
		read(Kb),
		retractall(curr_kb(_)),
		assert(curr_kb(Kb)),
		reconsult(Kb),
		menu,
		!.
		
do(2):- write('******************************START******************************'),
		nl,
		initialrules, %crea liste (iniziali e di servizio) con le regole presenti nella wm
		initialfacts, %crea liste (iniziali e di servizio) con i fatti presenti nella wm
		initindexing, %crea i predicati irule per ciascuna regola utili all'indicizzazione
		mainloop, %richiama il gestore principale
		nl,
		write('******************************FINISH******************************'),
		nl,
		menu,
		!.
		
do(3):- listing(fact),
		menu,
		!.
		
do(4):- listing(used_fact),
		menu,
		!.
		
do(5):- listing(rule),
		menu,
		!.
		
do(6):- listing(used_rule),
		menu,
		!.
do(7):- listing(irule),
		menu,
		!.
do(8):-initfacts(If), %recupera lista fatti iniziali
	   initializefacts(If), %ripristina i fatti allo stato iniziale
	   initializerules, %ripristina le regole allo stato iniziale
	   retractall(min_threshold(_)),
	   assert(min_threshold(0.5)), %fissa la soglia di completamento delle condizioni a 0.5 di default
	   assert(fireset([])),
	   
	   menu,
	   !.
do(9):-save,
	   menu,
	   !.
do(0). 

do(Chs):- nl,
		  write('Warning: wrong choice, type one of the number corresponding to the choices above please.'),menu.
