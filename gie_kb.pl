%Author: Nicola Milella
%Mat: 585459
%------------------------base di conoscenza utile per prove mirate----------------------

%fact(id,fatto,certezza)

fact(1,genitore(paolo,mario),0.4).
fact(2,genitore(mario,luca),0.3).
fact(3,genitore(luca,gigi),0.4).
fact(4,genitore(lidia,teresa),0.3).




%rule(id,testa,corpo,priorità,certezza)

rule(1,antenato(X,Y),or([and([genitore(X,Z),genitore(Z,Y)]),nonno(X,Y)]),6,1).
rule(2,conoscenti(X,Y),or([antenato(X,Y),amici(X,Y),parenti(X,Y)]),7,1).
rule(3,parenti(X,Y),or([genitore(X,Y),nonno(X,Y),antenato(X,Y)]),7,1).
rule(4,antenato(X,Y),or([nonno(X,Y),nonno(Y,X)]),9,1).
rule(5,parenti(X,Y),or([fratello(Y,X),genitore(X,Y)]),4,1).


% fatti chiedibili all'utente

demandable(nonno(X,Y)).
